#include <string>
#include <iostream>     
#include <fstream>      
#include <string>
#include <bitset>
#include <cstring>
#include <cstdlib>
#include <istream>
#include "imagemPPM.hpp"
#include "imagemPGM.hpp"

using namespace std;

int main(int argc, char ** argv) {
  int opcaoEscolhida,opcaoPPM;
      cout << "========================Programa de Esteganografia EP 01=======================" << endl;
      do{
      cout << "Escolha a opção correspondente ao tipo de imagem que deseja decifrar a mensagem:" << endl;
      cout << "\t\t1-Imagem formato PGM\n\t\t2-Imagem formato PPM\n\t\t3-Sair\n\t\tDigite a opção: ";
      cin >> opcaoEscolhida;
      switch(opcaoEscolhida){
          case 0:
             break;
          case 1:
            PGM *imagem1;
            imagem1=new PGM();
            imagem1->decifrarMensagem();
                  cout << "\n";
		        delete(imagem1);
            break;
          case 2:
            cout << "OPÇÃO 2 ESCOLHIDA -- > Decifrando imagem PPM" << endl;
            cout << "\t\t1-Filtro Vermelho\n\t\t2-Filtro Verde\n\t\t3-Filtro Azul\n\t\tDigite a opção:" ;
            cin >> opcaoPPM;   
            PPM *imagem2;
            imagem2=new PPM();
              switch(opcaoPPM){
                case 1:
                imagem2->aplicaFiltroRed();
                break;
                case 2:
                imagem2->aplicaFiltroGreen();
                break;
                case 3:
                imagem2->aplicaFiltroBlue();
                break;
              
            }
		        delete(imagem2);
            break;
          case 3:
            cout << "Foi escolhida a opcao para SAIR ! \n PROGRAMA ENCERRADO." << endl;
            break;          
          }
          cout << " \n"<< "\n" << endl;
  }while(opcaoEscolhida!=3);
    return 0;
}






