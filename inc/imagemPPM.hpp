#ifndef IMAGEMPPM_HPP
#define IMAGEMPPM_HPP

#include "imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class PPM :public Imagem{ 
private:
	int tamanhocabecalho;
public:

	PPM();	
	PPM(string nome);
	~PPM();
	void aplicaFiltroRed();
	void aplicaFiltroGreen();
	void aplicaFiltroBlue();
	 int getTamanhoCabecalho();
	 void calculaTamanhoCabecalho();
	 void setTamanhoCabecalho(int tamanhocabecalho);
};

#endif
