#ifndef IMAGEMPGM_HPP
#define IMAGEMPGM_HPP

#include "imagem.hpp"
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

class PGM : public Imagem { 
private:

	int tamanhocabecalho;	

public:
	PGM();
	PGM(string nome);	
	~PGM();
	void decifrarMensagem();
	//===============================================
     int getTamanhoCabecalho();
	 void calculaTamanhoCabecalho();
	 void setTamanhoCabecalho(int tamanhocabecalho);
};

#endif





